package main

import (
	"archive/tar"
	"compress/gzip"
	"flag"
	"io"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"time"
)

var flagvarA string
func init() {
	flag.StringVar(&flagvarA, "a", "", "path to archive dir\n./DAY02EX03 -a /data/archive /path/to/logs/some_application.log /path/to/logs/other_application.log")
}

func addFile(tw * tar.Writer, path string) error {
	file, err := os.Open(path)
	if err != nil {
		return err
	}
	defer file.Close()
	if stat, err := file.Stat(); err == nil {
		header := new(tar.Header)
		header.Name = path
		header.Size = stat.Size()
		header.Mode = int64(stat.Mode())
		header.ModTime = stat.ModTime()
		if err := tw.WriteHeader(header); err != nil {
			return err
		}
		if _, err := io.Copy(tw, file); err != nil {
			return err
		}
	}
	return nil
}

func timeStmp() string{
	return strconv.FormatInt(time.Now().Unix(),10)
}

func oneFileArg(i int, wg *sync.WaitGroup){
	defer wg.Done()
	var file *os.File
	var err error
	runtime.Gosched()
	sepPath := strings.Split(os.Args[i], "/")
	onlyNameFile := sepPath[len(sepPath) - 1]
	name := strings.TrimSuffix(onlyNameFile, filepath.Ext(onlyNameFile))
	if os.Args[1] == "-a"{
		file, err = os.Create(os.Args[2]+name + "_"+timeStmp()+ ".tar.gz")
		if err != nil {
			log.Fatalln(err)
		}
	}else{
		file, err = os.Create(name + "_"+timeStmp()+ ".tar.gz")
		if err != nil {
			log.Fatalln(err)
		}
	}
	defer file.Close()
	gw := gzip.NewWriter(file)
	defer gw.Close()
	tw := tar.NewWriter(gw)
	defer tw.Close()

	if err := addFile(tw, os.Args[i]); err != nil {
		log.Fatalln(err)
	}
	return
}

func main() {
	flag.Parse()
	wg := &sync.WaitGroup{}
	if flag.NFlag() == 0 {
		for i := 1; i < len(os.Args);i++ {
			wg.Add(1)
			go oneFileArg(i, wg)
		}
	}
	if os.Args[1] == "-a"{
		for i := 3; i < len(os.Args);i++ {
			wg.Add(1)
			go oneFileArg(i, wg)
		}
	}
	wg.Wait()
}