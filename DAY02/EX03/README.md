# Log rotation tool.
Log rotation" is a process when the old log file is archived and put away for storage so logs wouldn't pile up in a single file indefinitely.

Will create file `/path/to/logs/some_application_1600785299.tag.gz` where 1600785299 is a UNIX timestamp made from `some_application.log`'s
```
~$ ./DAY02EX03 /path/to/logs/some_application.log
```
Will create two tar.gz files with timestamps (one for every log) 
and put them into /data/archive directory
```
~$ ./DAY02EX03 -a /data/archive /path/to/logs/some_application.log /path/to/logs/other_application.log
```

build: `go build DAY02EX03.go`