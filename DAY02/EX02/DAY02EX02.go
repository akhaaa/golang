package main

import (
	"bufio"
	"fmt"
	"os"
	"os/exec"
	"runtime"
	"strings"
	"sync"
)

func runner(scanenr string, wg *sync.WaitGroup, justString string) {
	defer wg.Done()
	cmd, err := exec.Command("bash", "-c", justString + " " + scanenr).Output()
	if err != nil{
		fmt.Println(err)
		return
	}
	fmt.Print(string(cmd))
	runtime.Gosched()
}

func main() {
	var data []string
	var justString string
	wg := &sync.WaitGroup{}
	for i := 1; i < len(os.Args); i ++{
		data = append(data, os.Args[i])
	}
	justString = strings.Join(data," ")
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		wg.Add(1)
		go runner(scanner.Text(), wg, justString)
	}
	wg.Wait()
}