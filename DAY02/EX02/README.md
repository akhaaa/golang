### Let's implement a similar tool - in this exercise you'll need to write a utility that will:
1) treat all parameters as a command, like 'wc -l' or 'ls -la'
2) build a command by appending all lines that are fed to program's stdin as this command's arguments, then execute it. So if we run
```
~$ echo -e "/a\n/b\n/c" | ./DAY02EX02 ls -la
```
it should be an equivalent to running
```
~$ ls -la /a /b /c
```

build: `go build DAY02EX02.go`

run: `<some arg or cmd> | ./DAY02EX02 <some cmd>`

for example: `./DAY02EX00 -f -ext \'txt\' /tmp/ | ./DAY02EX02 ./DAY02EX01 -l`

will calculate line counts for all ".txt" files in `/tmp/` directory recursively.


or

`echo -e "222\n333\n444" | ./DAY02EX02 mkdir`

create dir 222, 333, 444

