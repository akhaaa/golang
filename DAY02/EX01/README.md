## Let's implement a `wc`-like utility to gather basic statistics about our files.

need to implement three mutually exclusive (only one can be specified at a time, otherwise an error message is printed) flags for your code: `-l` for counting lines, `-m` for counting characters and `-w` for counting words.

build: `go build DAY02EX01.go`

run: `./DAY02EX01 [-m | -l | -w] text.txt text2.txt ...`

If no flags are specified, `-w` behaviour should be used.