package main

import (
	"bufio"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"runtime"
	"strings"
	"sync"
)

var flagvarL string
var flagvarM string
var flagvarW string

func init() {
	flag.StringVar(&flagvarL, "l", "", "print counting lines\n./DAY02EX01 -l text.txt text1.txt ...")
	flag.StringVar(&flagvarM, "m", "", "print counting characters\n./DAY02EX01 -m text.txt text1.txt ...")
	flag.StringVar(&flagvarW, "w", "", "print counting words\n./DAY02EX01 -w text.txt text1.txt ...")
}

func wordCounter(i int, wg *sync.WaitGroup){
	defer wg.Done()
	content, err := ioutil.ReadFile(os.Args[i])
	if err != nil {
		log.Fatal(err)
	}
	text := string(content)
	scanner := bufio.NewScanner(strings.NewReader(text))
	scanner.Split(bufio.ScanWords)
	count := 0
	for scanner.Scan() {
		count++
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading input:", err)
	}
	fmt.Println(count, os.Args[i])
	runtime.Gosched()
}
func wordCount(f int, wg *sync.WaitGroup){
	for i := f ; i < len(os.Args);i++ {
		go wordCounter(i, wg)
	}
}

func lineCounter(i int, wg *sync.WaitGroup){
	defer wg.Done()
	file, err := os.Open(os.Args[i])
	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()
	scanner := bufio.NewScanner(file)
	var count int
	for scanner.Scan() {
		count = count + 1
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	fmt.Println(count, os.Args[i])
	runtime.Gosched()
}

func lineCount(wg *sync.WaitGroup){
	for i := 2; i < len(os.Args);i++ {
		go lineCounter(i ,wg)
	}
}

func charCounter(i int, wg *sync.WaitGroup){
	defer wg.Done()
	content, err := ioutil.ReadFile(os.Args[i])
	if err != nil {
		log.Fatal(err)
	}
	text := string(content)
	r := []rune(text)
	fmt.Println(len(r), os.Args[i])
	runtime.Gosched()
}

func charCount(wg *sync.WaitGroup){
	for i := 2; i < len(os.Args); i++ {
		go charCounter(i, wg)
	}
}

func main() {
	flag.Parse()
	wg := &sync.WaitGroup{}
	if len(os.Args) == 1{
		fmt.Println("No arg and flag, need text.txt as arg")
		os.Exit(1)
	}
	if flag.NFlag() == 0 {
		wg.Add(len(os.Args) - 2)
		wordCount(1, wg)
	}
	if flag.NFlag() > 1{
		fmt.Println("Error count flag")
		os.Exit(1)
	}
	if os.Args[1] == "-w" && flag.NFlag() != 0 {
		wg.Add(len(os.Args) - 2)
		wordCount(2, wg)
	}
	if os.Args[1] == "-l"{
		wg.Add(len(os.Args) - 2)
		lineCount(wg)
	}
	if os.Args[1] == "-m"{
		wg.Add(len(os.Args) - 2)
		charCount(wg)
	}
	wg.Wait()
}