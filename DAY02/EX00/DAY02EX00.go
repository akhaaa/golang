package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"sync"
)

func checkLenArg()  int{
	lenArg := len(os.Args)
	return lenArg
}

func checkExt(extenIndex int){
	ext := os.Args[extenIndex]
	if string(ext[0]) != "'" || string(ext[len(ext) -1]) != "'"{
		fmt.Println("error extension after -ext, examle: -ext 'go'\nIf it not work, try with -etx \\'go\\'")
		os.Exit(1)
	}
	if len(ext) == 2{
		fmt.Println("seems extension arg is empty")
		os.Exit(1)
	}
}

func checkFilePath() bool{
	if checkLenArg() == 1 {
		fmt.Printf("count flag error\nUsage:  %s [-d | -f | -f -ext | -sl] </path/to/dir>\n\t%s </path/to/dir> For use as with -f -d -sl\n\t-d Find only directories\n\t-s Find only symlinks\n\t" +
			"-f [-ext 'extension'] Find only file\n\t-ext Find only file extension (works ONLY when use -f) example: -f -ext \\'json\\' /go\n", os.Args[0],os.Args[0])
		os.Exit(1)
	}
	fiPath := os.Args[checkLenArg() - 1]
	if string(fiPath[0]) != "." && string(fiPath[0]) != "/"{
		fmt.Println("error path, examle: /foo or /foo/bar or . or /")
		os.Exit(1)
	}
	return true
}

func checkFlagforExt(extenSearch, fileSearch int){
	if extenSearch == 1 && fileSearch == 0{
		fmt.Printf("\"-ext\" use with \"-f\"\nUsage:  %s [-d | -f | -f -ext | -sl] </path/to/dir>\n\t%s </path/to/dir> For use as with -f -d -sl\n\t-d Find only directories\n\t-s Find only symlinks\n\t" +
			"-f [-ext 'extension'] Find only file\n\t-ext Find only file extension (works ONLY when use -f) example: -f -ext \\'json\\' /go\n", os.Args[0],os.Args[0])
		os.Exit(1)
	}
}


func checkFlagus(val string) bool{
	if val == os.Args[0] || val == os.Args[checkLenArg()-1]{
		return true
	}
	if val == "-f" || val == "-d" || val == "-sl" || val == "-ext" {
		return true
	}
	return false
}

func checkFlager() {
	var ext int
	for _, val := range os.Args {
		if val == os.Args[0] {
			continue
		}
		if val == os.Args[checkLenArg()-1] {
			continue
		}
		if val == "-ext"{
			ext = 1
		}
		if ext == 1 && val == os.Args[checkLenArg()-2] {
			continue
		}
		if checkFlagus(val) == false {
			fmt.Printf("have incorrect flag\nUsage:  %s [-d | -f | -f -ext | -sl] </path/to/dir>\n\t%s </path/to/dir> For use as with -f -d -sl\n\t-d Find only directories\n\t-s Find only symlinks\n\t" +
				"-f [-ext 'extension'] Find only file\n\t-ext Find only file extension (works ONLY when use -f) example: -f -ext \\'json\\' /go\n", os.Args[0],os.Args[0])
			os.Exit(1)
		}

	}
}

func checkFlag() (fileSearch, directorySearch, symlSearch, extenSearch int){
	var extenIndex int

	for ind, val := range os.Args{
		switch val {
		case "-f":
			fileSearch = 1
		case "-d":
			directorySearch = 1
		case "-sl":
			symlSearch = 1
		case "-ext":
			extenSearch = 1
			extenIndex = ind
			checkExt(extenIndex + 1)
		}
	}
	checkFlagforExt(extenSearch, fileSearch)
	return
}

func readerZeroFlag(wg *sync.WaitGroup) {
	defer wg.Done()
	err := filepath.Walk(os.Args[checkLenArg() - 1],
		func(path string, info os.FileInfo, err error) error {
			link, _ := os.Readlink(path)
			if len(link) == 0{
				fmt.Println(path)
				runtime.Gosched()
			}
			if len(link) > 0{
				linkus, eror := filepath.EvalSymlinks(path)
				if eror != nil{
					fmt.Println(path, "->", "[broken]")
					runtime.Gosched()
				}
				if len(linkus) > 0{
					fmt.Println(path, "->", link)
					runtime.Gosched()
				}
			}
			return nil
		})
	if err != nil {
		log.Println(err)
	}
}

func readerDirFlag(wg *sync.WaitGroup){
	defer wg.Done()
	err := filepath.Walk(os.Args[checkLenArg() - 1],
		func(path string, info os.FileInfo, err error) error {
			link, _ := os.Readlink(path)
			if len(link) == 0{
				if info.IsDir() == true{
					fmt.Println(path)
					runtime.Gosched()
				}
			}
			return nil
		})
	if err != nil {
		log.Println(err)
	}
}

func readerSymlFlag(wg *sync.WaitGroup){
	defer wg.Done()
	err := filepath.Walk(os.Args[checkLenArg() - 1],
		func(path string, info os.FileInfo, err error) error {
			link, _ := os.Readlink(path)
			if len(link) > 0{
				linkus, eror := filepath.EvalSymlinks(path)
				if eror != nil{
					fmt.Println(path, "->", "[broken]")
					runtime.Gosched()
				}
				if len(linkus) > 0{
					fmt.Println(path, "->", link)
					runtime.Gosched()
				}
			}
			return nil
		})
	if err != nil {
		log.Println(err)
	}
}

func readerFileFlag(wg *sync.WaitGroup){
	defer wg.Done()
	err := filepath.Walk(os.Args[checkLenArg() - 1],
		func(path string, info os.FileInfo, err error) error {
			link, _ := os.Readlink(path)
			if len(link) == 0{
				if info.IsDir() == false{
					fmt.Println(path)
					runtime.Gosched()
				}
			}
			return nil
		})
	if err != nil {
		log.Println(err)
	}
}

func readerExtFlag(wg *sync.WaitGroup){
	defer wg.Done()
	err := filepath.Walk(os.Args[checkLenArg() - 1],
		func(path string, info os.FileInfo, err error) error {
			link, _ := os.Readlink(path)
			if len(link) == 0{
				if info.IsDir() == false{
					ee := os.Args[checkLenArg() - 2]
					ee = strings.Replace(ee, "'", ".", 1)
					ee = strings.TrimSuffix(ee, "'")
					exten := strings.HasSuffix(path, ee)
					if exten == true{
						fmt.Println(path)
						runtime.Gosched()
					}
				}
			}
			return nil
		})
	if err != nil {
		log.Println(err)
	}
}

func main(){

	checkFilePath()
	fileSearch, directorySearch, symlSearch, extenSearch := checkFlag()
	if checkLenArg() >= 3{
		checkFlager()
	}
	wg := &sync.WaitGroup{}

	if checkLenArg() == 2{
		wg.Add(1)
		go readerZeroFlag(wg)
	}
	if directorySearch == 1{
		wg.Add(1)
		go readerDirFlag(wg)
	}
	if symlSearch == 1{
		wg.Add(1)
		go readerSymlFlag(wg)
	}
	if fileSearch == 1 && extenSearch != 1{
		wg.Add(1)
		go readerFileFlag(wg)
	}
	if extenSearch == 1{
		wg.Add(1)
		go readerExtFlag(wg)
	}
	wg.Wait()
}