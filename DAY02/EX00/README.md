# **`find`-like utility using Go.**
Finding all files/directories/symlinks recursively in directory **/foo**

build: `go build DAY02EX00.go`

run: `./DAY02EX00 /foo`

Usage:  

        ./DAY02EX00 [-d | -f | -f -ext | -sl] </path/to/dir>
        ./DAY02EX00 </path/to/dir> For use as with -f -d -sl
        -d Find only directories
        -s Find only symlinks
        -f [-ext 'extension'] Find only file
        -ext Find only file extension (works ONLY when use -f) example: -f -ext \'json\' /go
