We have a bunch of integer numbers, application will read it from a standard input, separated by newlines. Four major statistical metrics that we can derive from this data, by default we can print all of them as a result, for example, like this:

```
Mean: 8.2
Median: 9.0
Mode: 3
SD: 4.35
```
1) Input data may or may not be sorted.
2) Median is a middle number of a sorted sequence if its size is odd, and an average between two middle ones if their count is even.
3) Mode is a number which is occurring most frequently, and if there are several, the smallest one among those is returned.
4) You can use both population and regular standard deviation, whichever you prefer.
5) Calling someone "average" can be mean.

User to be able to choose specifically, which of these four parameters to print.

build: `go build poolday00.go`

run: `cat text.txt | ./poolday00 -sd -mode -median -mean`