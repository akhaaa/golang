package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"sort"
	"strconv"
)

func sdcalc(data []int) {
	sr := meancalc2(data)
	tr := int(sr)
	var dataint []int
	var datapow []float64
	var dataint2 []int
	for _, x := range data {
		dataint = append(dataint, x-tr)
	}

	for _, v := range dataint {
		datapow = append(datapow, math.Pow(float64(v), 2))
	}

	for _, r := range datapow {
		tt := int(r)
		dataint2 = append(dataint2, tt)
	}

	res := meancalc2(dataint2)
	ff := float64(res)
	ff = math.Sqrt(ff)
	fmt.Printf("SD: %.2f\n", ff)

}

func meancalc(data []int) float32 {
	var d int = 0
	for _, v := range data {
		d += v
	}
	ee := float32(d) / float32(len(data))
	fmt.Printf("Mean: %.2f\n", ee)
	return ee
}
func meancalc2(data []int) float32 {
	var d int = 0
	for _, v := range data {
		d += v
	}
	ee := float32(d) / float32(len(data))
	return ee
}

func mapkey(m map[int]int, value int) (min int) {
	var lol []int
	for k, v := range m {
		var key int
		if v == value {
			key = k
			lol = append(lol, key)
		}
	}
	sort.Ints(lol)
	min = lol[0]

	return
}

func modecalc(data []int) {
	mode := make(map[int]int)
	for x := 0; x < len(data); x++ {
		mode[data[x]] = 0
	}
	for x := 0; x < len(data); x++ {
		for key, value := range mode {
			if key == data[x] {
				mode[key] = value + 1
			}
		}
	}
	var maxvalue int = 0
	for x := 0; x < len(data); x++ {
		if maxvalue < mode[data[x]] {
			maxvalue = mode[data[x]]
		}
	}

	min := mapkey(mode, maxvalue)
	min2 := float32(min)
	fmt.Printf("Mode: %.2f\n", min2)
}

func mediancalc(data []int) {
	if len(data)%2 == 0 {
		ee := len(data)/2 - 1
		r := float32((float32(data[ee]) + float32(data[ee+1])) / 2)
		fmt.Printf("Median: %.2f\n", r)
	} else {
		med := float32(data[(len(data) / 2)])
		fmt.Printf("Median: %.2f\n", med)
	}
}

func resolve(sd int, mean int, mode int, median int, datanumber []int) {
	if sd != 0 {
		sdcalc(datanumber)
	}
	if mean != 0 {
		meancalc(datanumber)
	}
	if mode != 0 {
		modecalc(datanumber)
	}
	if median != 0 {
		mediancalc(datanumber)
	}

}

func inputdata() []int {
	var data []int
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		step, err := strconv.Atoi(scanner.Text())
		if err != nil {
			fmt.Println("ERROR: data is not valid.\nNeed only number on a new line")
			os.Exit(1)
		}
		data = append(data, step)
	}
	sort.Ints(data)
	return data
}

func checkflag() (int, int, int, int) {

	var sd int = 0
	var mean int = 0
	var mode int = 0
	var median int = 0

	fi, _ := os.Stdin.Stat()

	if fi.Size() == 0 {
		fmt.Println("Empty file :(")
		os.Exit(1)
	}

	if fi.Mode()&os.ModeNamedPipe == 0 {
		fmt.Printf("no pipe and data stdin :(\ntry: text.txt | %s [-sd -mean -mode -median]\n", os.Args[0])
		os.Exit(1)
	}

	//fmt.Printf("The file is %d bytes long", fi.Size())

	for _, v := range os.Args {
		if v == "-sd" {
			sd = 1
		} else if v == "-mean" {
			mean = 1
		} else if v == "-mode" {
			mode = 1
		} else if v == "-median" {
			median = 1
		}
	}

	if len(os.Args) == 1 {
		fmt.Printf("flag error\ntry: text.txt | %s [-sd -mean -mode -median]\n", os.Args[0])
		os.Exit(1)
	} else if len(os.Args) == 2 {
		if os.Args[1] != "-sd" && os.Args[1] != "-mean" && os.Args[1] != "-mode" && os.Args[1] != "-median" {
			fmt.Printf("flag error\ntry: text.txt | %s [-sd -mean -mode -median]\n", os.Args[0])
			os.Exit(1)
		}
	} else if len(os.Args) == 3 {
		if os.Args[2] != "-sd" && os.Args[2] != "-mean" && os.Args[2] != "-mode" && os.Args[2] != "-median" {
			fmt.Printf("flag error\ntry: text.txt | %s [-sd -mean -mode -median]\n", os.Args[0])
			os.Exit(1)
		}
	} else if len(os.Args) == 4 {
		if os.Args[3] != "-sd" && os.Args[3] != "-mean" && os.Args[3] != "-mode" && os.Args[3] != "-median" {
			fmt.Printf("flag error\ntry: text.txt | %s [-sd -mean -mode -median]\n", os.Args[0])
			os.Exit(1)
		}
	} else if len(os.Args) == 5 {
		if os.Args[4] != "-sd" && os.Args[4] != "-mean" && os.Args[4] != "-mode" && os.Args[4] != "-median" {
			fmt.Printf("flag error\ntry: text.txt | %s [-sd -mean -mode -median]\n", os.Args[0])
			os.Exit(1)
		}
	} else if len(os.Args) == 6 {
		fmt.Printf("flag error\ntry: text.txt | %s [-sd -mean -mode -median]\n", os.Args[0])
		os.Exit(1)
	}

	return sd, mean, mode, median
}

func main() {

	sd, mean, mode, median := checkflag()
	datanumber := inputdata()
	resolve(sd, mean, mode, median, datanumber)
}
