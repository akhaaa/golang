module gopool/DAY03/EX04

go 1.15

require (
	github.com/auth0/go-jwt-middleware v0.0.0-20200810150920-a32d7af194d1 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/olivere/elastic/v7 v7.0.21
)
