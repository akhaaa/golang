package main

import (
	"encoding/json"
	"fmt"
	"github.com/auth0/go-jwt-middleware"
	"github.com/dgrijalva/jwt-go"
	"log"
	"net/http"
	"time"
)

type Claims struct {
	jwt.StandardClaims
}
type Token struct {
	TokenStr string `json:"token"`
}
const (
	APP_KEY = "blablabla"
)
func Signin(w http.ResponseWriter, r *http.Request) {

	expirationTime := time.Now().Add(5 * time.Minute)
	claims := &Claims{
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString([]byte(APP_KEY))
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	tokk := Token{TokenStr: tokenString}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(tokk)
	fmt.Println(tokenString)
}

func main() {
	var gg KafeDocs
	ff, num := ReadBD(&gg,0, 10)
	fmt.Println(num)
	//fmt.Println(ff)

	http.HandleFunc("/api/get_token", Signin)
	http.Handle("/api/recommend", AuthMiddleware(http.HandlerFunc(ExampleHandler)))
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusCreated)
		json.NewEncoder(w).Encode(ff)
	})
	log.Fatal(http.ListenAndServe(":8888", nil))
}

func AuthMiddleware(next http.Handler) http.Handler {
	if len(APP_KEY) == 0 {
		log.Fatal("HTTP server unable to start, expected an APP_KEY for JWT auth")
	}
	jwtMiddleware := jwtmiddleware.New(jwtmiddleware.Options{
		ValidationKeyGetter: func(token *jwt.Token) (interface{}, error) {
			return []byte(APP_KEY), nil
		},
		SigningMethod: jwt.SigningMethodHS256,
	})
	return jwtMiddleware.Handler(next)
}

func ExampleHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(`{"status":"ok"}`)
}

