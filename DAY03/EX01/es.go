package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/olivere/elastic/v7"
	"log"
	"time"
)

type KafeDocs struct {
	Id       int      `json:"id"`
	Name     string   `json:"name"`
	Address  string   `json:"address"`
	Phone    string   `json:"phone"`
	Location GeoPoint `json:"location"`
}

type GeoPoint struct {
	Lat float64 `json:"lat"`
	Lon float64 `json:"lon"`
}

type Store interface {
	GetPlaces(limit int, offset int) ([]KafeDocs, int)
}

func ReadBD(reader Store, limit int, offset int) ([]KafeDocs, int) {
	return reader.GetPlaces(limit, offset)
}
func (reader *KafeDocs) GetPlaces(limit int, offset int) ([]KafeDocs, int) {
	ctx := context.Background()
	client, err := elastic.NewClient(
		elastic.SetURL("http://localhost:9200"),
		elastic.SetHealthcheckInterval(5*time.Second),
	)
	if err != nil {
		log.Fatal(err)
	}
	termQuery := elastic.NewMatchAllQuery()
	searchResult, err := client.Search().
		Index("places").
		Query(termQuery).
		From(limit).Size(offset).
		Pretty(true).
		Do(ctx)
	if err != nil {
		log.Fatal(err)
	}

	var result []KafeDocs
	if searchResult.Hits.TotalHits.Value > 0 {
		for _, hit := range searchResult.Hits.Hits {
			err := json.Unmarshal(hit.Source, &reader)
			if err != nil {
				log.Fatal(err)
			}
			result = append(result, *reader)
		}
	} else {
		fmt.Print("Found no pleces\n")
	}
	return result, len(result)
}
