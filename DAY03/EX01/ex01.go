package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
)

func main() {
	var gg KafeDocs
	ff, num := ReadBD(&gg, 0, 15000)
	fmt.Println(num)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		t, err := template.ParseFiles("homepage.html")
		if err != nil {
			log.Print("template parsing error: ", err)
		}
		err = t.Execute(w, ff)
		if err != nil {
			log.Print("template executing error: ", err)
		}
	})
	log.Fatal(http.ListenAndServe(":8888", nil))
}
