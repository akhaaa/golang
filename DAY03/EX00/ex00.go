package main

import (
	"context"
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"reflect"
	"strconv"
	"time"

	"github.com/olivere/elastic/v7"
)

func init() {
	flag.Bool("c", false, "create Index")
	flag.Bool("d", false, "delete Index")
	flag.Bool("p", false, "put data for index")
	flag.Parse()
}

const indexName = "places"

type KafeDocs struct {
	Id       int      `json:"id"`
	Name     string   `json:"name"`
	Address  string   `json:"address"`
	Phone    string   `json:"phone"`
	Location GeoPoint `json:"location"`
}

type GeoPoint struct {
	Lat float64 `json:"lat"`
	Lon float64 `json:"lon"`
}

func delIndex(client *elastic.Client) {
	ctx := context.Background()

	deleteIndex, err := client.DeleteIndex("places").Do(ctx)
	if err != nil {
		fmt.Println("client.CreateIndex error", err)
		os.Exit(2)
	}
	if !deleteIndex.Acknowledged {
		fmt.Println("exist")
	}
}

func createIndex(client *elastic.Client) {
	mapping := `{
		"settings":{
			"number_of_shards":1,
			"number_of_replicas":1
		},
		"mappings":{
			"properties":{
				"id": {
					"type":"long"
				},
				"name": {
					"type":"text"
				},
				"address": {
	    			"type":"text"
				},
				"phone": {
	    			"type":"text"
				},
     			"location": {
       			"type": "geo_point"
     			}
			}
		}
	}`

	ctx := context.Background()
	createIndex, err := client.CreateIndex("places").BodyString(mapping).Do(ctx)
	if err != nil {
		fmt.Println("client.CreateIndex error", err)
		os.Exit(2)
	}
	if !createIndex.Acknowledged {
		fmt.Println("exist")
	}

}

func putInIndex(client *elastic.Client) {
	ctx := context.Background()
	indices := []string{indexName}
	existService := elastic.NewIndicesExistsService(client)
	existService.Index(indices)
	exist, err := existService.Do(ctx)

	if err != nil {
		fmt.Println("existService.Do ERROR:", err)
		os.Exit(2)
	} else if exist == false {
		fmt.Println("The index", indexName, "doesn't exist.")
		fmt.Println("Create the index, and then run the Go script again")
	} else if exist == true {
		fmt.Println("Index name:", indexName, " exists!")

		var docs []KafeDocs

		fmt.Println("docs TYPE:", reflect.TypeOf(docs))

		recordFile, err := os.Open("/Users/aleksandr/go/src/gopool/DAY03/EX00/data.csv")
		if err != nil {
			fmt.Println("os.Open error:", err)
			return
		}
		defer recordFile.Close()

		reader := csv.NewReader(recordFile)
		reader.FieldsPerRecord = -1
		reader.Comma = '\t'

		header, err := reader.Read()
		if err != nil {
			fmt.Println("reader.Read() error:", err)
			return
		}
		fmt.Printf("Headers : %v \n", header)

		for i := 0; ; i++ {
			record, err := reader.Read()
			if err == io.EOF {
				break
			} else if err != nil {
				fmt.Println("An error encountered1 ::", err)
				return
			}

			gg1, _ := strconv.Atoi(record[0])
			gg2, _ := strconv.ParseFloat(record[4], 64)
			gg3, _ := strconv.ParseFloat(record[5], 64)

			docID := gg1

			newDoc1 := KafeDocs{Id: gg1, Name: record[1], Address: record[2], Phone: record[3], Location: GeoPoint{Lat: gg3, Lon: gg2}}

			idStr := strconv.Itoa(docID)
			put1, err := client.Index().Index("places").Id(idStr).BodyJson(newDoc1).Do(context.Background())
			if err != nil {
				fmt.Println("client.Index() ERROR:", err)
			}
			fmt.Printf("Indexed places %s to index %s, type %s, status %s\n", put1.Id, put1.Index, put1.Type, put1.Result)
		}

	}
}

func main() {
	log.SetFlags(0)

	client, err := elastic.NewClient(
		elastic.SetURL("http://localhost:9200"),
		elastic.SetHealthcheckInterval(5*time.Second),
	)
	if err != nil {
		fmt.Println("elastic.NewClient() ERROR:", err)
		log.Fatalf("quiting connection..")
	} else {
		fmt.Println("client:", client)
		fmt.Println("client TYPE:", reflect.TypeOf(client))
	}
	if len(os.Args) >= 2 {
		if os.Args[1] == "-d" {
			delIndex(client)
		}
		if os.Args[1] == "-c" {
			createIndex(client)
		}
		if os.Args[1] == "-p" {
			putInIndex(client)
		}

	}

}
