package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

func main() {
	var gg KafeDocs
	ff, num := ReadBD(&gg, 0, 15000)
	fmt.Println(num)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusCreated)
		json.NewEncoder(w).Encode(ff)
	})
	log.Fatal(http.ListenAndServe(":8888", nil))
}
