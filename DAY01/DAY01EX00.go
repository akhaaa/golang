package main

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

type xmlJsonStatham struct {
	XMLName xml.Name `xml:"recipes" json:"-"`
	Cake    []struct {
		Name      string `xml:"name" json:"name"`
		Stovetime string `xml:"stovetime" json:"time"`
		Item      []struct {
			Itemname  string `xml:"itemname" json:"ingredient_name"`
			Itemcount string `xml:"itemcount" json:"ingredient_count"`
			Itemunit  string `xml:"itemunit,omitempty" json:"ingredient_unit,omitempty"`
		} `xml:"ingredients>item" json:"ingredients"`
	} `xml:"cake" json:"cake"`
}

type DBReader interface {
	readDB(file []byte, fileFlag int)
}

func readDB(reader DBReader, jsonFile []byte, fileFlag int){
	reader.readDB(jsonFile, fileFlag)
}

func (reader *xmlJsonStatham) readDB(file []byte, fileFlag int) {

	if fileFlag == 1 {
		err := json.Unmarshal(file, &reader)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		xmlOut, err := xml.MarshalIndent(reader, "", "    ")
		if err != nil {
			log.Fatal("Failed to generate xml", err)
		}
		fmt.Printf("%s\n", string(xmlOut))
	}else{
		err := xml.Unmarshal(file, &reader)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		jsonOut, err := json.MarshalIndent(reader, "", "    ")
		if err != nil {
			log.Fatal("Failed to generate json", err)
		}
		fmt.Printf("%s\n", string(jsonOut))
	}
}

func checkAll() (file []byte, fileFlag int){
	if len(os.Args) != 3{
		fmt.Printf("count flag error\ntry: %s -f <file.xml or file.json>\n", os.Args[0])
		os.Exit(1)
	}
	if os.Args[1] != "-f"{
		fmt.Printf("first flag error\ntry: %s -f <file.xml or file.json>\n", os.Args[0])
		os.Exit(1)
	}
	xmlflag := strings.HasSuffix(os.Args[2], ".xml")
	jsonflag := strings.HasSuffix(os.Args[2], ".json")
	if xmlflag != true && jsonflag != true{
		fmt.Printf("second flag error\ntry: %s -f <file.xml or file.json>\n", os.Args[0])
		os.Exit(1)
	}

	if xmlflag == true{
		fileFlag = 0
	}else{
		fileFlag = 1
	}

	file, err := ioutil.ReadFile(os.Args[2])
	if err != nil{
		fmt.Printf("file open error\ntry: %s -f <file.xml or file.json>\n", os.Args[0])
		os.Exit(2)
	}
	return file, fileFlag
}

func main(){

	file, fileFlag := checkAll()
	var base xmlJsonStatham
	readDB(&base, file, fileFlag)
}