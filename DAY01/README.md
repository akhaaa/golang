## **Convert `.json -> .xml` and `.xml -> .json`**

build: `go build DAY01EX00.go`

run: `./DAY01EX00 -f database.json`
or run: `./DAY01EX00 -f database.xml`